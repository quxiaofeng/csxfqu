---
layout: page
title : Computer Vision People
header : Computer Vision People
---
{% include JB/setup %}


Zhang Lin
-----------------------
<http://sse.tongji.edu.cn/linzhang/>

+ Friend
+ Inventor of Finger Knuckle Print
+ Riesz Transform Expert
+ Great Programmer

Richard Hartley
-----------------------
<http://users.cecs.anu.edu.au/~hartley/>

+ 3D
+ [Multiple View Geometry In Computer Vision](http://www.robots.ox.ac.uk/~vgg/hzbook/), [in Amazon](http://www.amazon.com/Multiple-View-Geometry-Computer-Vision/dp/0521540518), [in Douban](http://book.douban.com/subject/1841346/)

Hongdong Li
-----------------------
<http://users.cecs.anu.edu.au/~hongdong/>

+ 3D

CARL BOETTIGER
-----------------------
<http://www.carlboettiger.info/wordpress/>

+ about pandoc

Stephen Sinclair
-----------------------
<http://www.music.mcgill.ca/~sinclair/content/>

+ about pandoc

丕子
-----------------------
<http://www.zhizhihu.com/>

+ funny and some common information

Chiyuan Zhang
-----------------------
<http://pluskid.org/index.html>
<http://freemind.pluskid.org/>

+ funny + cute blog style

Xie Yihui
-----------------------
<http://yihui.name/cn/>
<http://yihui.name/en/>

+ R + HTML


Yan Ping
-----------------------
<http://chen.yanping.me/cn/blog/2012/03/13/pandoc/>

+ Pandoc + R
