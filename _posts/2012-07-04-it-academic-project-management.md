---
layout: post
tags : [project management, academic]
---
    
## To-Do List

+ Read _PMBOK 4th edition in Chinese and English_
+ Read _The Project Manager's Desk Reference_
+ To select some proper procedures for current projects

## Project Management Software and Gant Chart

## Important Elements on Project Management
+ Resources
  + People
  + Time
  + Cost
  + Code
  + Paper
  + Technology
  + Theory
+ Schedule
+ Productive Rate Evaluation
+ Critical Path
+ Goal
+ Risk

# Open Source Project Management Server

+ [kunagi](http://kunagi.org/)
