---
layout: post
title: Ergonomics
tags: [door knob, design, device]
description: The size of the device is very important. Which size is most confortable for people to use? It is very hard to tell. Here we collect some infos about the ergonomics research, which studys general parameters of human being.
---
###  Ergonomics Website
1. [Processforusability.co.uk](http://www.processforusability.co.uk/index.htm)  
2. [Ergonomics of hand held devices for industrial use](http://www.processforusability.co.uk/PIC54webslides/Slides/Ergonomics_of_hand-held_devices.htm)  
3. [a table of anthropometric data ](http://www.technologystudent.com/designpro/ergo1.htm)  
4. [Human Touch Sensing for Actuators in Haptic Interfaces](http://www-cdr.stanford.edu/touch/actuators/Actuators_Sensing_Summary.html)  
