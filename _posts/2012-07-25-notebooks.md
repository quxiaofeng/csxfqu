---
title: Computers
layout: post
tags : [notebook]
description: A webpage started by the disputed student notebook ownership 2012. Summarizing all the computers I have. The performance tests, OS, and usage guides. And also thoughts about how to upgrade or improve and maintain a perfect working condition.
---

## [ThinkPad x230(i7-3520)](http://www.hknotebook.com/hkust2012/product/main.jsp?view=product&pid=5&brand=lenovo)
#### _A DREAM COMES TRUE_
    
>   3-year Intenational Warranty  
>   i7-3520M, 4MB Cache/2.9GHz/1600MHz FSB  
>   **2 x 8GiB** DDR3 1600MHz (Max. 16GB)  
>   **128 GB M5Pro SSD**  
>   **128 GB M4 mSATA SSD**    
>   **16 GB SD card**, as auxilary storage. 30 MiB/s writing speed  
>   12.5", 1366 x 768  
>   720p HD Webcam  
>   Integrated Graphics  
>   Lithium Ion 6-cell/Up to 9.9 hours  
>   1.34kg  

- Plextor M5 Pro 128GB SSD Speed Test with AS SSD Benchmark(right after the system installation)    
![SSD speed test]({{ site.url }}/images/ssd_speedtest.png)    
- 4 GiB RAM Disk Speed Test using DataRam    
![RAM disk speed test]({{ site.url }}/images/ramdisk_speedtest.png)    
- Windows Experience Index with SSD / 16 GiB RAM / 2 GiB Display RAM    
![windows 7 experience index]({{ site.url }}/images/windows_experience.png)    
- Trying to find a friend in HKUST/HKBU/CITYU. **Solved, THANKS to ZHM.**

#### System Test after Adding a mSATA SSD

*Some test are too slow to carry out.*

- Main SSD with almost full files    
The performance is still very good as just bought.    
![PLEXTOR_PX-128M5_2012_10_15]({{ site.url }}/images/PLEXTOR_PX-128M5_2012_10_15.jpg)    
- RamDisk is still the best, over 10 times of SSD    
![RamDisk_2012_10_15]({{ site.url }}/images/RamDisk_2012_10_15.jpg)    
- Crucial M4 128GB mSATA is slightly weaker than M5P. Not to mention it is empty.    
![M4-CT128M4SSD3_A_2012_10_15]({{ site.url }}/images/M4-CT128M4SSD3_A_2012_10_15.jpg)    
And also here is [a review of this mSATA SSD](http://www.storagereview.com/micron_realssd_c400_msata_ssd_review).    
- Mobile HDD(the original hdd came with x230) with USB 3.0    
The difference with SSD is obvious especially the random test.    
![HITACHI_HTS72505_2012_10_15]({{ site.url }}/images/HITACHI_HTS72505_2012_10_15.jpg)     
- SanDisk Extreme 16GB Flash Disk    
Extrodinary read, poor write and worst random.    
![SanDisk_Extreme__2012_10_15]({{ site.url }}/images/SanDisk_Extreme__2012_10_15.jpg)    
- SD Card 16GB    
Worst at any field.    
![Ricoh_SD_Disk_De_2012_10_15]({{ site.url }}/images/Ricoh_SD_Disk_De_2012_10_15.jpg)    

#### [XP Installation Guide on IVB Platform](http://news.mydrivers.com/1/237/237098_all.htm)

#### x230 tips
- [The build and analysis](http://www.tpuser.idv.tw/wp/)    
- [Review in depth](http://www.notebookcheck.net/Lenovo-ThinkPad-X230-2306-2AU-Laptop-Review.75317.0.html)    
- A confirmed [removal](http://forums.lenovo.com/t5/T400-T500-and-newer-T-series/T530-does-not-make-typical-ThinkPad-Power-Status-Beeps/td-p/788571/highlight/false) of the power beep, and a VB script is attached as an alternate solution      
- x230 user [experience and optimization](http://littlenine1221.pixnet.net/blog/post/91040924-%5B%E6%96%B0%E5%93%81%E5%88%86%E4%BA%AB%5D-lenovo-x230-%2B-sandisk-480gb-ssd-%2B-16gb-ram-%2Bg)     
- x230i mSATA SSD replacement [guide](http://www.5i01.cn/topicdetail.php?f=240&t=2840788&last=37435297)     

#### SSD infos    
+ **Plextor M5 Pro 128G** in [**Hong Kong**](http://www.price.com.hk/product.php?p=143173), [Performance test](http://www.chiphell.com/thread-531991-1-1.html)    
+ famous [thessdreview.com](http://thessdreview.com/)    
+ [SSD performance review](http://www.expreview.com/19604-all.html)    
+ [Several Tips about SSD](http://diy.pconline.com.cn/cpu/study_cpu/1203/2722291_all.html)     
+ [Windows 7 tweaks](http://article.pchome.net/content-1387324.html)    
+ [Win 7 optimizing](http://moonwulk.blogspot.hk/2010/07/windows-7-ssd.html)    
+ [SSD 2012 roundup](http://www.behardware.com/art/imprimer/860/)     
+ [**SSD 4 KiB Alignment Performance Test**](http://www.seekxiu.com/article.aspx?id=12568)   

#### Demystifying External Hard Disk    
+ [German Medion](http://item.taobao.com/item.htm?id=18461252774) USB 3.0 external hard disk case    

## [Students Notebook Ownership Program 2012](http://www.hkepc.com/forum/viewthread.php?tid=1829216&extra=page%3D1)    
+ [PolyU](http://www.hknotebook.com/polyu2012/)    
  + [Acer](http://www.hknotebook.com/polyu2012/acer/index.jsp)    
  + [Samsung](http://www.hknotebook.com/polyu2012/samsung/index.jsp)    
+ [Notebook Performance Cost comparison Chart]({{ site.url }}/images/cpu.xlsx)    

## Several considerations    
- SSD for performance and data security    
- i7 cpu for performance    
- Light weight for mobility    

### Current condition   
- [Computer comparison]({{ site.url }}/images/cpus.xlsx)   
- Thinkpad X230   
  - main working env   
  - Windows 7 64bit    
  - VirtualBox Ubuntu 12.04 64bit   
- Two PCs in office    
  - one can not copy to and from remote access    
  - the QQ downloader can not be activated during remote accessing    
- One PC at home    
  - fast enough    
  - maybe add some mobile HDDs in it, just keep things together.    
- P8010    
  - Using by Jocelyn for office and entertainment purpose.    
- M2010   
  - On Sale  

## Ubuntu 12.04 and Optimizing

### An apple Virtual Machine for IOS development?  

+ [Easily Run Mac OS X 10.8 Mountain Lion Retail on PC with VMware Image – Simple Steps](http://www.sysprobs.com/easily-run-mac-os-x-10-8-mountain-lion-retail-on-pc-with-vmware-image)    
+ [SecurAble](http://www.grc.com/securable.htm), test processor securaty features.    

