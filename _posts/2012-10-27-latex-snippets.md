---
layout : post
tags : [writing]
description: Useful latex, especially beamer snippets.
---

# Snippets

## TODO notes

+ [todonotes package](http://www.texample.net/tikz/examples/todo-notes/)

## Enumeration over several frames [Beamer]

+ [discussion](http://www.latex-community.org/forum/viewtopic.php?f=44&t=9362&sid=288bcd91ac509f9b951d7d25ca094e8d&start=10)

## LaTeX CV

+ [YOUR CURRICULUM IN LATEX](http://stefano.italians.nl/archives/5), A tutorial of how to make custom CV.

# Other Resources A Powerful FAQ blog

+ [LaTeX-3.14159265](http://blog.sina.com.cn/wangzhaoli11)    
+ [GNU TeXmacs](http://www.texmacs.org/), another latex suite.    

