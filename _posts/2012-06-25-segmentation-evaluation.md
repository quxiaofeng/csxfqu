---
layout: post
tags : [segmentation]
title: Image Segmentation Evaluation
description: The ground truth evaluation of image segmentation.
---
    
## First Round    
    
### [__Survey A__]({{ site.url }}/manual/surveya.html) (50 image sets)    

### [__Survey B__]({{ site.url }}/manual/surveyb.html) (50 image sets)    

### [__Survey C__]({{ site.url }}/manual/surveyc.html) (50 image sets)    

### [__Survey D__]({{ site.url }}/manual/surveyd.html) (50 image sets)    
    
## Second Round    
    
### [__Survey E__]({{ site.url }}/manual/surveye.html) (50 image sets)       

### [__Survey F__]({{ site.url }}/manual/surveyf.html) (50 image sets)     

### [__Survey G__]({{ site.url }}/manual/surveyg.html) (50 image sets)    

### [__Survey H__]({{ site.url }}/manual/surveyh.html) (50 image sets)    



