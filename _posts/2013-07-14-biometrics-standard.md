---
layout      : post
tage        : [biometrics, standard]
title       : Biometrics Standard 生物特征识别标准
description : Standards, organizations in biometrcis.
---

## Fingerprint

### [CDEFFS](http://fingerprint.nist.gov/standard/cdeffs/), Committee to Define an Extended Fingerprint Feature Set

CDEFFS was a committee chartered to define a quantifiable, standard method of characterizing the information content of a fingerprint or other friction ridge image. The work of CDEFFS resulted in the Extended Feature Set (EFS) specification included in the [ANSI/NIST ITL-1 2011 standard "Data Format for the Interchange of Fingerprint, Facial, & Other Biometric Information"](http://biometrics.nist.gov/cs_links/standard/AN_ANSI_1-2011_standard.pdf). The ANSI/NIST ITL standards are the basis for biometric and forensic standards used around the world, including the FBI's EBTS and Interpol's INT-I, among others.
