---
layout: post
tags: [life, hongkong, shenzhen, news, video, shopping]
description: Websites for everyday life.
---
## [PolyU](http://www.polyu.edu.hk/cpa/polyu/index.php) Links

+ [Campus Map](http://www.polyu.edu.hk/fmo/eMap/map.php)    
+ [Email - Connect](https://login.live.com/login.srf?cbcxt=out&vv=910&wa=wsignin1.0&wtrealm=urn:federation:MicrosoftOnline&wctx=wa%3Dwsignin1.0%26rpsnv%3D2%26ct%3D1340602759%26rver%3D6.1.6206.0%26wp%3DMBI_KEY%26wreply%3Dhttps:%252F%252Fwww.outlook.com%252Fowa%252F%26id%3D260563%26CBCXT%3Dout)   
+ [Research Portal](https://www40.polyu.edu.hk/rostudportal/tologin.do)   
+ [Sports Facilities Booking System](https://www40.polyu.edu.hk/saosport/)
+ [Library](http://www.lib.polyu.edu.hk/)    

### [Computing Department](http://www.comp.polyu.edu.hk/en/home/index.php)  

+ [Webmail](https://webmail.comp.polyu.edu.hk/roundcubemail/)     
+ [Intranet](https://intranet.comp.polyu.edu.hk/)  
+ [iPrint Printer Driver](http://csnw03.comp.polyu.edu.hk/ipp)    

## Life

<table>
<tbody>
    <tr>
        <th>Weather   </th>
        <td><ul><li><a href="http://www.hko.gov.hk/wxinfo/currwx/fndc.htm">Observatory</a></li></ul></td>
    </tr>
    <tr>
        <th>Banks     </th>
        <td><ul><li><a href="https://e-banking1.hangseng.com">HangSeng</a></li>
                <li><a href="https://mobile.hkbea-cyberbanking.com/servlet/FRLogon">BEA</a></li>
                <li><a href="https://ibank.standardchartered.com.hk/nfs/login.htm">Standard Chartered</a></li>
                <li><a href="https://www.ppshk.com/hkt/revamp2/Chinese/LoginPage.html">PPS</a></li></ul></td>
    </tr>
    <tr>
        <th>Currencies</th>
        <td><ul><li><a href="http://www.xe.com/zh/">XE</a></li></ul></td>
    </tr>
    <tr>
        <th>Cellphone </th>
        <td><ul><li><a href="https://www.three.com.hk/appCS2/my3Account.do?lang=chi&appId=appCSCheckBill">Three Company</a></li></ul></td>
    </tr>
    <tr>
        <th>VPN</th>
        <td><ul><li><a href="http://www.vpncloud.me/">VPN Cloud</a></li></ul></td>
    </tr>
</tbody>
</table>

## Forums or Blogs
<ul>
    <li><a href="http://www.newsmth.net">水木社区 SMTH</a></li>
    <li><a href="http://lifehacker.com">Life Hacker</a></li>
    <li><a href="http://blog.ifeng.com/4177985.html">偶尔一笔(凤凰网博客)</a>, 桥本隆则 日籍华裔 (for world)</li>
</ul>

## News
<table>
<tbody>
    <tr>
        <th>China PR</th>
        <td><a href="http://news.qq.com">腾讯(Tencent)</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www.huanqiu.com">环球网</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www.cnbeta.com">cnBeta</a></td>
    </tr>
    <tr>
        <th>Hong Kong SAR</th>
        <td><a href="http://www.ifeng.com">凤凰网(iFeng.com)</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://news.yahoo.com">Yahoo News</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www.wenxuecity.com">文学城(wenxuecity)</a></td>
    </tr>
    <tr>
        <th>English</th>
        <td><a href="http://www.bbc.co.uk/news">BBC NEWS</a>, british view.</td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www3.nhk.or.jp/nhkworld">NHK World</a>, live english news from Japan.</td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www.nytimes.com">New York Times</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://rt.com">Russian Times</a></td>
    </tr>
    <tr>
        <th></th>
        <td><a href="http://www.washingtontimes.com">The Washington Times</a></td>
    </tr>
</tbody>
</table>

## Online Music

<table>
<tbody>
    <tr><th>Listening</th><td><a href="http://douban.fm/">豆瓣FM douban.fm</a></td><td>Personalized</td></tr>
    <tr><th>         </th><td><a href="http://www.yinyuetai.com/">音悦台  </a></td><td>HD MTV</td></tr>
</tbody>
</table>

## Online Video Site

<table>
<tbody>
    <tr><th>Video 2.0</th><th>World   </th><td><a href="http://www.youtube.com">YouTube</a></td></tr>
    <tr><th>         </th><th>Mainland</th><td><a href="http://www.youku.com">优酷</a></td></tr>
    <tr><th>         </th><th>        </th><td><a href="http://www.tudou.com">土豆</a></td></tr>
    <tr><th>LiveTV   </th><th>Mainland</th><td><a href="http://www.fengyunzhibo.com">风云直播</a></td></tr>
    <tr><th>         </th><th>        </th><td><a href="http://www.topway.cn">威视网</a></td></tr>
    <tr><th>         </th><th>        </th><td><a href="http://www.iqiyi.com">爱奇艺</a></td></tr>
    <tr><th>         </th><th>Taiwan  </th><td><a href="http://sugoideas.com">sugoideas</a></td></tr>
    <tr><th>         </th><th>Russian </th><td><a href="http://rt.com/on-air">Russian TImes</a></td></tr>
    <tr><th>         </th><th>American</th><td><a href="http://beelinetv.com">Beeline TV</a></td></tr>
    <tr><th>Cartoon  </th><th>        </th><td><a href="http://www.letv.com">乐视网</a></td></tr>
    <tr><th>         </th><th>        </th><td><a href="http://www.bilibili.tv">哔哩哔哩</a></td></tr>
</tbody>
</table>

## Video Download Site

<table>
<tbody>
    <tr><th>Download</th><td>       </td><td><a href="http://www.yyets.com">人人影视 YYets</a>, HD movies</td></tr>
    <tr><th>        </th><td>       </td><td><a href="http://www.dygod.org">电影天堂</a>, movie and episodes</td></tr>
    <tr><th>        </th><td>       </td><td><a href="http://www.ttmeiju.com">天天美剧</a>, Download through QQdownload
    <tr><th>        </th><td>torrent</td><td><a href="http://isohunt.com">isohunt</a>, bt seeds</td></tr>
    <tr><th>        </th><td>       </td><td><a href="http://thepiratebay.se">Pirate Bay</a>, bt seeds</td></tr>
    <tr><th>        </th><td>ed2k   </td><td><a href="http://simplecd.me">SimpleCD</a>, ed2k resources</td></tr>
    <tr><th>        </th><td>tools  </td><td><a href="http://keepvid.com">KEEPVID</a>, download youtube videos</td></tr>
    <tr><th>        </th><td>       </td><td><a href="http://www.flvcd.com">硕鼠</a>, download youku videos</td></tr>
    <tr><th>        </th><td>OS     </td><td><a href="http://zxkh19501.blog.163.com">无约而来</a></td></tr>
</tbody>
</table>

## Cinemas
<table>
<tbody>
    <tr><th>Hong Kong </th>
        <td>A Movie <a href="http://media.netvigator.com/media/bse/media/home/MVE/MVE_MOV.jsp">Navigator</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.goldenharvest.com">Golden Harvest</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.uacinemas.com.hk/eng/main/HomePage">UA</a></td></tr>
    <tr><th>Shenzhen  </th>
        <td><a href="http://www.szxhfilm.com">Century Starlight / 中影世纪星辉</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://theater.mtime.com/China_Guangdong_Province_Shenzen">Mtime.com</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://movie.douban.com/nowplaying/shenzhen">Douban Movies</a></td></tr>
</tbody>
</table>

## Shopping

### IT Products
<table>
<tbody>
    <tr><th>Official  </th>
        <td><a href="http://www.fortress.com.hk">Fortress</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.price.com.hk">Price.com.hk</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://store.apple.com/hk-zh">Apple Store</a></td></tr>
    <tr><th>Used      </th>
        <td><a href="http://dcfever.com/trading/listing.php?category=3">DCFever</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://hk.auctions.yahoo.com/hk/23336-category.html?.r=1344488639">Yahoo.hk</a></td></tr>
    <tr><th>Comparisons</th>
        <td><a href="http://www.amazon.com">Amazon</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.taobao.com">Taobao</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.360buy.com/">Jingdong</a></td></tr>
    <tr><th>          </th>
        <td><a href="http://www.smzdm.com/">SMZDM</a>, a Buyers' Guide</td></tr>
    <tr><th>          </th>
        <td><a href="http://www.chiphell.com">Chip Hell</a>, a cutting-edge review site</td></tr>
</tbody>
</table>

### Consmetic    
+ [Bonjour](http://www.bonjourhk.com/tc/main.aspx)
+ [SaSa](http://web1.sasa.com/SasaWeb/tch/sasa/home.jsp)

### Promotions    
+ [GetJetso.com](http://www.getjetso.com/)

### Track Your Express    
+ [Track with Number](http://www.yto5.cn/)

## SiteIsDownOrBlocked    
+ Use [this site](http://isdownorblocked.com/) to chek other sites.    
+ [Host auto update script](http://qiujj.com/static/28001-fgqi.txt)    

Extension of Stay for Student and dependent (签注延期)
-----------------------------------------------------------

### PolyU Testimonial Letter

1.  Fill out, print and sign the RC/19 [*download*](http://www.polyu.edu.hk/ro/forms/FormRC19.doc) from.
2.  Pay HK$ 30.00(one testimonial copy) with the printed RC/19 form (in order to show your purpose) in Cashier Office at VA205 ([*check office hours*](http://www.polyu.edu.hk/fo/FO_Web/index.php?page=9)) in the Shaw Amenities Building. And get the receipt.
3.  Take the receipt, your HK ID card and [Two-way Permit](http://en.wikipedia.org/wiki/Two-way_Permit) / [往来港澳通行证](http://zh.wikipedia.org/wiki/%E4%B8%AD%E5%8D%8E%E4%BA%BA%E6%B0%91%E5%85%B1%E5%92%8C%E5%9B%BD%E5%BE%80%E6%9D%A5%E6%B8%AF%E6%BE%B3%E9%80%9A%E8%A1%8C%E8%AF%81) to Research Office ([*check office hours*](http://www.polyu.edu.hk/ro/newROContact_stud.html)) at M501 in Li Ka Shing Tower.
4.  Wait at most four working days to get this. And there will be a letter tell you what to do next.

### HK Visa Label (in person)

Generally follow [*this guide*](http://www.immd.gov.hk/en/faq/extend-stay-terminate-sponsorship-transfer-endorsement.html#non_local_extend) / [*办理方法*](http://www.immd.gov.hk/sc/faq/extend-stay-terminate-sponsorship-transfer-endorsement.html#non_local_extend).

#### Take all files listed below to Immigration Department ([*check address and office hours*](http://www.immd.gov.hk/sc/services/addresses/branch-office.html)).

##### Student / 学生 (according to [*this page*](http://www.gov.hk/sc/residents/immigration/nonpermanent/extensionstaynpr/esgen.htm) )

-   Fill out, print and sign the form ID91 ([*download*](http://www.immd.gov.hk/pdforms/id91.pdf)).
-   HK ID card and a copy (only front)
-   Two-way Permit and copies of **all important pages**
  -   HK visa label page
  -   China Mainland visa label page
  -   Last page with a photo (maybe two pages, if you are using two permits)
  -   Last exit and entry page
-   Testimonial letter from RO
-   Bank monthly statement (print out the electronic statement)

##### Dependent / 受养人 (according to [*the same page*](http://www.gov.hk/sc/residents/immigration/nonpermanent/extensionstaynpr/esgen.htm) )

-   Fill out, print and sign the form ID91 ([*download*](http://www.immd.gov.hk/pdforms/id91.pdf)).
-   Fill out, print and sign the form ID481A ([*download*](http://www.immd.gov.hk/pdforms/id481ac.pdf)).
-   Fill out, print and sign the form ID481B ([*download*](http://www.immd.gov.hk/pdforms/id481bc.pdf)).
-   HK ID card and a copy (only front)
-   Two-way Permit and copies of *all important pages*
    -   HK visa label page
    -   China Mainland visa label page
    -   Last page with a photo
    -   Last exit and entry page

#### Take the receipt and wait for seven working days

#### Bring the receipt, two-way permit and HK$ 160 to collect visa label

### HK Visa Label (on-line)

Less than four weeks before last visa label expiring.

Upload scanned files.

### China Mainland Visa Label (Luohu Agency/广东省公安厅深圳出入境签证办事处)

> 地址：深圳市罗湖区南湖路1001号广东省公安厅深圳出入境签证办事处综合楼  
> 咨询电话：0755-82329899  
> 投诉电话：0755-82327700  
> 受理时间：星期一至星期六 9:00-12:00 14:00-17:00  
> 办证期限：换领证件15个工作日、续办签注10个工作日  
> 收费标准（人民币）   
> 证件费100元  
> 1年内（含1年）多次有效签注100元  
> 1年以上2年以下（含2年）多次有效签注150元  
> 2年以上3年以下（含3年）多次有效签注200元  
> 3年以上多次有效签注300元  

1. Take a Guangdong police certified photo (￥40)
   + Cut one photo
   + Get the certificate paper
2. Take all ID cards and copies to Luohu Agency
   + China ID card and a copy (both sides)
   + HK ID card and a copy (front)
   + Two-way permit
      - last photo page
      - HK visa page
      - China visa page


### China Mainland Visa Label ([*HKCTS*](http://www.ctshk.com/zhengjian/rencai/rencai.htm)/[*港中旅*](http://www.ctshk.com/zhengjian/rencai/rencai.htm))

Omitted.
