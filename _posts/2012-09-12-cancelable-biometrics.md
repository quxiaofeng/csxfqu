---
layout: post
tags: [biometrics, academic]
description: A survey of cancelable biometrics.
---

## Websites

+ [scholarpedia.org](http://www.scholarpedia.org/article/Cancelable_biometrics)
+ [wikipedia.org](http://en.wikipedia.org/wiki/Biometrics#Cancelable_biometrics)
+ [IBM cancelable face biometrics](http://researcher.watson.ibm.com/researcher/view_project_subpage.php?id=1914)

## Papers

