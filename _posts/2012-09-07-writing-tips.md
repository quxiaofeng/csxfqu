---
layout: post
tags : [writing, paper, academy]
description: Some tips for writing
---

## Structure for Writing Device Paper
+ Introduction
+ Prior Work
+ System Description
+ Theory or Method
+ Experimental Result and Discussion
+ Conclusion and Futher Reserch

## Elements
+ motivation: Why this study is important.
+ description: How you plan, design and do it, and the result.
+ explanation: Why you do it this way, and what does the data/result mean.
+ justification: How this method/data justify our points.
+ limitation: Is there some disadvantages or limits.

## Research Coding Tips

+ [Patterns for research in machine learning](http://hunch.net/?p=2562)    
+ [Patterns for Research in Machine Learning](http://arkitus.com/PRML/)    
+ [Principles of Research Code](http://www.theexclusive.org/2012/08/principles-of-research-code.html)    
+ [Software design patterns for Machine Learning R&D](http://news.ycombinator.com/item?id=4384317)    

