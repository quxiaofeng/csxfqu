---
layout: post
tags : [coding, software]
description: Websites and softwares for coding.
---

## Reference

+ [A Little C Primer](http://www.vectorsite.net/tscpp.html)   
+ [C/C++ reference](http://en.cppreference.com/w/)  
+ [SS64](http://ss64.com/index.html), Command line reference: Database and Operating Systems  
+ [Perl Doc](http://perldoc.perl.org/)  
+ [Programming CookBook](http://pleac.sourceforge.net/)  
+ [Cheet-Sheets.org](http://www.cheat-sheets.org/)  

## Source Code Management

+ [__github__](https://github.com/quxiaofeng) for public repos     
+ [__bitbucket__](https://bitbucket.org/quxiaofeng) for private repos     
+ [__gitcafe__](http://gitcafe.com/quxiaofeng) for mainland China access    
+ [__gitcd__](http://gitcd.com/user.htm?name=quxiaofeng) unlimited private repos and org repos    
+ gitlab, interested in this server    
+ [hg](http://en.wikipedia.org/wiki/Mercurial) is another great version control software. And this is a [tip page in Chinese](http://zh-hgtip.readthedocs.org/en/latest/)

## IDEs  

+ [Visual Studio](http://e5.onthehub.com/WebStore/OfferingsOfMajorVersionList.aspx?ws=d0ea765b-816f-e011-971f-0030487d8897&vsro=8&pmv=540786e5-43e4-de11-a13b-0030487d8897&cmi_mnuMain=bdba23cf-e05e-e011-971f-0030487d8897), The Hong Kong Polytechnic University (PolyU) has become a member of the Microsoft Dynamics Academic Alliance since 2007.    
+ Matlab    
+ Qt Creator     
  
## Libraries

+ [Qt SDK](http://qt-project.org/downloads)     
  - Qt 4.8.1    
+ [OpenCV](http://www.opencv.org)      
+ [Intel Threading Building Blocks](http://threadingbuildingblocks.org/)    
+ [TinyXML](http://www.grinninglizard.com/tinyxml/)    
  - [Analysis](http://panpan.blog.51cto.com/489034/104961/)    
  - [Some Summary](http://www.cnblogs.com/freecoder/archive/2006/08/07/TinyXmlStapleA.html)    
+ [jQeury User Interface](http://jqueryui.com/)    
+ [REL](http://imaginatio.github.com/REL/), a Regular Expression composition Library    
  - [10 Java Regexp Examples](http://www.hdqzzz.com/?p=51)

## Tools  

+ [CMake](http://www.cmake.org/)    
+ [Git](http://git-scm.com/)    
[Other GUI git frontends](http://maketecheasier.com/6-useful-graphical-git-client-for-linux/2012/01/18)    
  
## Languages  

+ [Ruby](http://www.ruby-lang.org/en/)    
  - [rubyinstaller](http://rubyinstaller.org/)    
+ [Python](http://www.python.org/download/releases/)   
+ Perl    

## Code Reviews and Analysis  

+ Source Insight for code reading and analysis    
  - from [SimpleCD](http://simplecd.org/id/175769)    
  - Source Insight 3.5.0064 [Setup.exe](ed2k://|file|%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.Si3564Setup.exe|5267968|33e259d0b0783bbcf2dc8c744535137e|h=uddt4fswbxal53rggydl5gutm5mcy2l3|/)    
  - Source Insight 3.5.0064 [sn.txt](ed2k://|file|%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.%5BSource.Insight.3.5.0064%5D.sn.txt|19|9cb3097c1386926b48260f6ef92c529b|h=mxms2uuceo5jreutn6wbrdi53dqmicb6|/)    
  - [Guide](ed2k://|file|SourceInsight%E4%BB%A3%E7%A0%81%E7%BC%96%E5%86%99%E8%BE%85%E5%8A%A9%E5%B7%A5%E5%85%B7V1.0%E7%9A%84%E4%BD%BF%E7%94%A8.rar|83280|8a3e502abcae10f5f5f2c53332f1d336|h=3sgpolkqkwygcddnd66kbmcyaldaz7l2|/)    
+ [Understand](http://www.scitools.com/) for code reading and analysis    
  - from [SimpleCD](http://simplecd.org/id/2925198)    
  - [Understand v3.0.615](ed2k://|file|%5B%E4%BB%A3%E7%A0%81%E9%98%85%E8%AF%BB%E5%B7%A5%E5%85%B7%5D.Scientific.Toolworks.Understand.v3.0.615.Incl.Keygen-Lz0.zip|107664445|c0f897e3a1cb7e2f6b903e7b186d9358|h=vgarpfnbwpqsdgmy4tlh2sl7sy4hjywx|/)    
  - [Understand v3.0.615 x64](ed2k://|file|%5B%E4%BB%A3%E7%A0%81%E9%98%85%E8%AF%BB%E5%B7%A5%E5%85%B7%5D.Scientific.Toolworks.Understand.v3.0.615.X64.Incl.Keygen-Lz0.zip|129783765|316b5e721b693fbce4a769c495baf6d2|h=p23dhgowtxlaohexkj5455krvpai2c2n|/)    
+ [StarUML](http://staruml.sourceforge.net/en/) for object-oriented design and analysis    
  - [Guide](ed2k://|file|UMLChina%E8%AE%B2%E5%BA%A7%E5%BD%95%E9%9F%B3%E5%92%8C%E5%B9%BB%E7%81%AF20080528%E9%82%B1%E9%83%81%E6%83%A0%E5%BC%80%E6%BA%90StarUML%E5%BB%BA%E6%A8%A1%E5%AE%9E%E6%88%98.rar|76500557|553afbaaf1113bc8664a7d91e0a3618c|h=hzci6suxy3caoupk6p6i4sja53vkurha|/)    
+ [FreeMind](http://freemind.sourceforge.net/wiki/index.php/Main_Page) for mindmapping    
  - [FreemindMMX Git Patch](http://redmine.ossxp.com/redmine/projects/freemind-mmx/wiki/IndexEn)    
  - [freemind-hacks](https://github.com/ossxp-com/freemind-hacks)    
  - [Freeplane](http://freeplane.sourceforge.net/wiki/index.php/Main_Page)    
+ [Doxygen](http://www.doxygen.nl/), auto doc generator.    
  - [A Chinese Tutorial](http://www.fmddlmyy.cn/text21.html)    
  - [Doxygen Document Style](http://ticktick.blog.51cto.com/823160/188674)    
  - [An Example](http://blog.csdn.net/abcwangdragon/article/details/1496943)    

## Mathematics

+ [gnuplot](http://www.gnuplot.info/)    
+ [QtiPlot](http://soft.proindependent.com/qtiplot.html)    
+ [SciDAVis](http://scidavis.sourceforge.net/about/index.html)    

## Test Virtual Machines

+ [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## [Writing Tools]({{ site.url }}{% post_url 2012-08-04-writing-tools %})

## Downloading

+ [All opensource softwares](http://ftp.heanet.ie/mirrors/)

## Microsoft DreamSpark

+ [DreamSpark](http://e5.onthehub.com/WebStore/ProductsByMajorVersionList.aspx?ws=d0ea765b-816f-e011-971f-0030487d8897&vsro=8&JSEnabled=1)

## Online Judge

+ [Zhejiang University Online Judge](http://acm.zju.edu.cn/onlinejudge/showProblemsets.do)


