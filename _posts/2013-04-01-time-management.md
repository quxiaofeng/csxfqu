---
layout : post
tags : [time management, slide]
title : 26 Time Management Hacks I Wish I&#39;d Known at 20
description: A slide I like and need
---

+ 26 Time Management Hacks I Wish I'd Known at 20

<iframe src="http://www.slideshare.net/slideshow/embed_code/17820376?rel=0" 
width="597" height="486" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" 
style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" 
allowfullscreen webkitallowfullscreen mozallowfullscreen>
</iframe>

+ [If You’re Busy, You’re Doing Something Wrong: The Surprisingly Relaxed Lives of Elite Achievers](http://calnewport.com/blog/2011/11/11/if-youre-busy-youre-doing-something-wrong-the-surprisingly-relaxed-lives-of-elite-achievers/)


