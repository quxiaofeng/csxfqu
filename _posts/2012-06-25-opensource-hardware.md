---
layout: post
tags : [chip, circuit]
title: Open Source Hardware
description: Opensource in another field.
---

## Guides and Wikis    
    
- [A guide (in Chinese)](http://www.52solution.com/article/articleinfo/id/80009270)    
- [OpenXC Platform](http://openxcplatform.com/)    
- [Open-source hardware wiki](http://en.wikipedia.org/wiki/Open-source_hardware)    
- [Open-source hardware wiki (in Chinese)](http://zh.wikipedia.org/wiki/%E9%96%8B%E6%BA%90%E7%A1%AC%E4%BB%B6)    
- [Arduino](http://www.arduino.cc/)    
- [Arduino wiki](http://en.wikipedia.org/wiki/Arduino)    
- [Make Arduino](http://makezine.com/arduino/)    
- [Qi hardware wiki](http://en.wikipedia.org/wiki/Qi_hardware)     
- [Open Design Circuits](http://www.opencollector.org/history/OpenDesignCircuits/reinoud_index.html)     
- [Survey of Open Source Hardware (in Chinese)](http://maker.eefocus.com/archives/1295)    
- [Frankencamera](http://graphics.stanford.edu/papers/fcam/)    
- [Another Survey](http://blog.21ic.com/user1/8413/archives/2012/88556.html)    
    
## Groups     

- [arduino.hk](http://arduino.hk/)   
- [Open Hardware Summit](http://summit.oshwa.org/)    
- [Bug Labs](http://www.buglabs.net/ford-buglabs)    
- [Open Source Hardware OSHW.org](http://freedomdefined.org/OSHW)   

## Mainland China    
    
- [Oriental Hardware Research Center](http://www.orihard.com/ku.asp)    

## Chips

- [AM3359](http://www.ti.com/product/am3359)

![AM3359]({{ site.url }}/images/am3359.jpg)


## Shops

+ [DFRobot](http://blog.dfrobot.com.cn/)
