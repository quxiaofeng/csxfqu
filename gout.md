---
layout: default
title: Gout
date: The disease, medicines and treatment
---
<article class ="post">


# [痛风](http://zh.wikipedia.org/wiki/%E7%97%9B%E9%A3%8E)[(Gout)](http://en.wikipedia.org/wiki/Gout)

+ Medicine
    - [别嘌醇](http://zh.wikipedia.org/wiki/%E5%88%AB%E5%98%8C%E9%86%87), English name [Allopurinol](http://en.wikipedia.org/wiki/Allopurinol)  __Allergic, STOP__
    - [苯溴马隆](http://baike.baidu.com/view/243323.htm)[(Benzbromarone)](http://en.wikipedia.org/wiki/Benzbromarone)
    - [双氯芬酸钠]("http://zh.wikipedia.org/wiki/%E5%8F%8C%E6%B0%AF%E8%8A%AC%E9%85%B8%E9%92%A0")[(Diclofenac)](http://en.wikipedia.org/wiki/Diclofenac) is a nonsteroidal anti-inflammatory drug (NSAID) taken to reduce inflammation and as an analgesic reducing pain.
    - No Vit C pills
    
+ Fruit
    - Apple
    - Orange
    - Blueberry
    
+ Food
    - Neither meat nor fish


