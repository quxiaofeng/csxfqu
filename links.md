---
layout: default
title: Links
date: Shortcuts to daily life and work.
---
<article class ="post">

![My Lovely Son]({{ site.imageLink }})

### [Life]({{ site.url }}{% post_url 2012-08-09-life-in-hong-kong-and-shenzhen %})
+ [Joke]({{ site.url }}{% post_url 2012-10-27-jokes %})
+ [Kindle]({{ site.url }}{% post_url 2012-07-04-kindle-resources %})

<h3>Links</h3>
<h4>Code Repo</h4>
<ul>
    <li><a href="{{ site.follow.bitbucket }}" target="_blank"><i class="icon-random"></i>Bitbucket</a></li>
    <li><a href="{{ site.follow.github }}" target="_blank"><i class="icon-github"></i>GitHub</a></li>
</ul>
<h4>Academic Profile</h4>
<ul>
    <li><a href="{{ site.follow.gscholar }}" target="_blank"><i class="icon-book"></i>Google Scholar</a></li>
    <li><a href="{{ site.follow.linkedin }}" target="_blank"><i class="icon-linkedin"></i>LinkedIn</a></li>
    <li><a href="{{ site.follow.mendeley }}" target="_blank">&nbsp;<img src="http://www.mendeley.com/graphics/favicon.ico" width="12" height="12" />&nbsp;Mendeley</a></li>
</ul>
<h4>Social Network</h4>
<ul>
    <li><a href="{{ site.follow.facebook }}" target="_blank"><i class="icon-facebook"></i>Facebook</a></li>
    <li><a href="{{ site.follow.twitter }}" target="_blank"><i class="icon-twitter"></i>Twitter</a></li>
    <li><a href="{{ site.follow.weibo }}" target="_blank"><i class="icon-rss"></i>Weibo</a></li>
    <li><a href="{{ site.follow.tencent }}" target="_blank">&nbsp;<img src="http://mat1.gtimg.com/www/mb/favicon.ico" width="12" height="12" />&nbsp;Tencent</a></li>
    <li><a href="{{ site.follow.google }}" target="_blank"><i class="icon-google-plus"></i>Google Plus</a></li>
</ul>

### [Work Cal](https://www.google.com/calendar/embed?src=tqa202la1dnrb70ld90094d31c%40group.calendar.google.com&ctz=Asia/Hong_Kong)
<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=WEEK&amp;height=400&amp;wkst=2&amp;hl=en&amp;bgcolor=%23ffffff&amp;src=tqa202la1dnrb70ld90094d31c%40group.calendar.google.com&amp;color=%232F6309&amp;src=zh_cn.hong_kong%23holiday%40group.v.calendar.google.com&amp;color=%231B887A&amp;ctz=Asia%2FHong_Kong" style=" border-width:0 " width="100%" height="400" frameborder="0" scrolling="no"></iframe>
<p></p>
     
### [Notes]({{ site.url }}{% post_url 2012-06-28-jekyll-notes %}) about this blog

+ The source code of this blog is hosted in [<i class="icon-github-sign"></i>github](https://github.com/quxiaofeng/csxfqu).
+ This site is using [theOne theme](https://github.com/pizn/blogTheme). It is designed by [PIZn](http://www.pizn.net/14-11-2012/theone-blog-theme/). Thanks so much for this simple and clean style.     

### Reading History      
    
<script type="text/javascript" src="http://www.douban.com/service/badge/gBlueBird/?show=collection&amp;select=random&amp;n=6&amp;columns=3&amp;hidelogo=yes&amp;cat=book" ></script>
<p></p>
